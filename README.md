this is a back-end server for the attendance tracking project using node.js and mongoDB.

The server is deployed on Heroku, you can find the app here: 
https://attendance-tracking-iu.herokuapp.com/


To run the server locally, you can do the following:
1. clone the git repository to your local machine.
2. set up the node development envirnment: https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/development_environment
3. run the command ```nmp install``` to install the dependencies of the package.json file
4. run the command ```node server.js```
5. open the browser and go to the local url: http://localhost:3000/