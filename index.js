 var express = require("express");
 var bodyparser = require("body-parser");
 var PORT = process.env.PORT || 3000;
 var app = express();
 const mongoClient = require("mongodb").MongoClient
 const objectId = require("mongodb").ObjectId
 const bcrypt = require("bcrypt")

 app.use(bodyparser.json())
 app.use(express.json())

 const url = "mongodb+srv://hussein:JyamZrzAJa0GshBL@attendancetracking-vqf5g.mongodb.net/test?retryWrites=true&w=majority"

 mongoClient.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }, (err, client) => {
    if (err) return console.log("Error while connecting to mongo client: " + err)

    ////////
    /// All endpoints (to ensure db is connected)
    ////////

    const myDB = client.db('attendancetracking')
    const collection = myDB.collection("user")
    const attendance = myDB.collection("attendance")

    // testing the DB
    app.get("/users", (req, res) => {
        const query = {
            username: "Daniel"
        }
        
        collection.findOne(query, (err, result) => {
            if (result == null){
                res.send('No documents')
            } else{
                res.status(200).send(result);
            }
        })
    })

    // sign up method
    app.post("/signup", (req, res) => {

        const newUser = {
            name: req.body.name,
            email: req.body.email,
            password: req.body.password
        }
        const query = {email: newUser.email}
        collection.findOne(query, (err, result) => {
            
            if (result == null) {
                collection.insertOne(newUser, (err, result) => {
                    res.status(200).send()
                })
            } else {
                res.status(400).send()
            }
        })
    })

    // login method
    app.post("/login", (req, res) => {
        const query = {
            email: req.body.email,
            password: req.body.password
        }

        collection.findOne(query, (err, result) => {
            
            if (result != null){


                // generate verification code
                require('crypto').randomBytes(8, function(err, buffer) {
                    var verificationCode = buffer.toString('hex');
                    result.verificationCode = verificationCode
                  });


                require('crypto').randomBytes(48, function(err, buffer) {
                    var token = buffer.toString('hex');

                    result.token = token
                    result.is_verified = false
                    collection.updateOne(query, {$set: result}, (err, result) => {
                        console.log("Item updated")
                    })

                    res.status(200).send(JSON.stringify(result))

                  });

                
            } else {
                res.status(403).send("no such user")
            }
        }) 
    })


    // confirm verification code endpoint
    app.post('/confirm_verification', (req, res) => {

        if (req.body.role != "student")
                res.status(403).send("unauthorized user")
        else 
            {    
                const query = {
                    _id: objectId(req.body._id)
                }
                collection.findOne(query, (err, result) => {
                    if(result.verificationCode != req.body.code){
                            res.status(403).send("incorrect code")
                        }
                        else{

                            // student account is verified
                            result.is_verified = true
                            collection.updateOne(query, {$set: result}, (err, result) => {
                                console.log("Item updated")
                            })
                            res.status(200).send()
                        }
                })
            }
    })
    


    // get verification code for a given user
    app.post('/get_verification_info', (req, res) => {

        if (req.body.role != "doe_member")
                res.status(403).send("unauthorized user")
        else 
            {    
                const query = {
                    _id: objectId(req.body._id)
                }
                collection.findOne(query, (err, result) => {
                    if (result != null){
                        res.status(200).send(result.verificationCode)
                    }
                    else {
                        res.status(403).send("user not found")
                    }
                })
            }
    })



    

    // get the list of unverified users
    app.post('/get_unverified_students', (req, res) => {

        if (req.body.role != "doe_member")
                res.status(403).send("unauthorized user")
        else 
            {    
                const query = {
                    role: "student",
                    is_verified: false
                }
                collection.find(query).toArray((err, result) => {
                    if (result != null){
                        students = []
                        result.forEach((item) => {
                            console.log(item)
                            students.push([item["_id"], item["username"]]);
                        })
                            res.status(200).send(students)
                    }
                    else {
                        res.status(403).send("user not found")
                    }
                })
            }
    })






    // home
    app.get('/', (req, res) => {
        res.send('Attendance Tracking up and running RIGHT')
    })
    


    //  Request should contain role as teacher
    app.get("/attendancetracking", (req, res) => {
        // query contains datetime, beaconid
        var query = req.query;
        
        attendance.find(query).toArray(function(err, documents) {
            var output = '<html><header><title>Attendance List from DB</title></header><body>';
            output += '<h1>Attendance List retrieved from DB</h1>';
            output += '<table border="1"><tr><td><b>' + 'studentId' + '</b></td>' + '<td><b>' + 'datetime' + '</b></td>' + '<td><b>' + 'beaconid' + '</b></td></tr>';

            // process result list
            documents.forEach( function(item) {
                output += '<tr><td>' + item["studentid"] + '</td><td>' + item["datetime"] + '</td><td>' + item["beaconid"] + '</td></tr>';
            });
            // write HTML output (ending)
            output += '</table></body></html>'

            // send output back
            res.send(output);
        })
    })



    // Accepts DateTime and BeaconId to save attendance info of student
    // The request will including student information
    app.post("/attendancetracking", (req, res) => {
        const student = {
            studentid: req.body.studentid,
            datetime: req.body.datetime,
            beaconid: req.body.beaconid
        }
        attendance.findOne(student, (err, result) => {
            if (result != null) {
                res.status(200).send()
            } else {
                attendance.insertOne(student, (err, docs) => {
                    res.status(200).send()                
                })
            }
        })
    })

    app.listen(PORT, () => {
        console.log("list on port " + PORT)
    })
 })

